/*
  Rbc logic.
  Receives decoded user's console cmd and answer the response.

  Incoming data - user's console's user input (string)
  Outcoming data - valid js object (later will coded as JSON stringify)

*/

const yargs_parser = require('yargs-parser');
const util = require('util');
const EventEmitter = require('events').EventEmitter;
const DBG = require('../debug');

class RbcLogic {

    constructor(config={}) {
        this.help_desc = {
            rec:"Help for rec",
            stop:"Help for stop",
            ls:"Help for ls",
            get:"Help for get",
            rm:"Help for rm",
            conf:"Help for conf",
            status:"Help for status",
            debug:
            `Set the level of debug information are print at the console.
                debug 0 - only critical messages must be displayed.
                debug 7 - to display all the data including low level messages.
                debug - display the current level value.\n`,
            def: "Available commands: rec, stop, ls, status, get, rm, conf, help",
        };
        //this.device_id='';
        //this._activeDevice = null;
        this.dbg = config.dbg || console;
        this.devPool = config.devPool;
        this.consoleAgent = config.consoleAgent;
        this.setMaxListeners(1);
    }
/*
    setActiveDevice(_activeDevice) {
         this.activeDevice = _activeDevice;
    }
*/
    reqTemplate(command, args) {
        let out = {
            "command": command,
            "arguments": {},
        };
        if(undefined !== args) {
            out["arguments"] = args;
        }
        return out;
    }

    writeProcedure(mess) {
        this.emit('user_out', mess);
    }

    sendProcedure(mess, data_info) {
        if(undefined == data_info) {
            data_info = {
                data_type: 'cmd',
            }
        }
        this.emit('mess_send',mess, data_info);
    }

    /*
      users input string converts to js object to prepare to routing
    */
    parseUserInput(inp_str) {
        let parsed_str = yargs_parser(inp_str);
        parsed_str.cmd = parsed_str._[0] || '';
        // "_" (from yargs) has all the data, but we have idx >= 1 as arguments
        parsed_str.arg = parsed_str._.slice(1);
        return parsed_str;
    }

    getHelp(sub_cmd) {
        if( !Object.keys(this.help_desc).includes(sub_cmd)) {
            sub_cmd = 'def';
        }
        return this.help_desc[sub_cmd];
    }

    /*
      process user input to control the rbc
      return false if rbc doesn't send message
    */
    //procUserInput(deviceID, obj) {
    procUserInput(obj) {
        this.dbg.log(DBG.NOTICE,'procUserInput');
        this.dbg.log(DBG.INFORMATION,obj);
        let actId = this.devPool.getActiveDeviceId();
        let out = {
            'device-id' : actId,
            'command'   : obj.cmd,
            'arguments' : { } };
        //console.log(this.activeDevice);
        switch (obj.cmd) {
        case "quit":
            process.exit();
            break;
        case 'debug':
            if (obj.arg.length > 0) {
                let n = Number.parseInt(obj.arg[0]);
                this.dbg.setDebugLevel(n);
            }
            this.writeProcedure(`Debug level: ${this.dbg.getDebugLevel()}`);
            break;
        case "device":
            if (obj.arg.length > 0) {
                let id = obj.arg[0].toString();
                if(this.devPool.setActiveDeviceId(id)) {
                    this.consoleAgent.setPromptPrefix(id);
                    this.writeProcedure('');
                } else {
                    this.writeProcedure(`Device id: ${id} is absent!`);
                }
            } else {
                let devList = this.devPool.getDeviceIdList();
                this.writeProcedure(devList.join("\n"));
            }
            break;
        case "exit":
            this.writeProcedure(obj.cmd + " is not implemented!");
            break;
        case "conf":
            if (obj.arg.length >= 0) {
                this.devPool.setParamsForRealById(actId,{reqState:"CONF"});
                for(let i=0; i < obj.arg.length; i=i+2) {
                    if(undefined !== obj.arg[i+1]) {
                        out.arguments[obj.arg[i]] = obj.arg[i+1];
                    }
                }
                this.sendProcedure(out);
            }
            break;
        case "rm":
            if(undefined == obj.arg[0]) {
                this.writeProcedure("Unknown filename!");
                this.writeProcedure(this.getHelp(obj.cmd));
                return;
            }
            if('*' !== actId) {
                out.arguments = {"name":obj.arg[0]};
                this.devPool.setParams(actId,{reqState: 'RM'});
                this.sendProcedure(out);
            } else {
                this.writeProcedure("The operation is not confirmed for the broadcast mode!");
            }
            break;
        case "status":
            this.devPool.setParamsForRealById(actId,{reqState:"STATUS"});
            this.sendProcedure(out);
            break;
        case "start":
            if (obj.arg.length > 0) {
                out['arguments'] = {
                    'devices' : obj.arg.map(function(e) { return e.toString(); })
                };
            } else {
                out['arguments'] = { };
            }
            this.sendProcedure(out);
            break;
        case 'stop':
            this.sendProcedure(out);
            break;
        case "get":
            //console.log('proc get:',obj);
            // out = this.reqTemplate(obj.cmd,{"name": obj.arg[0]});
            out['arguments'] = {data_type: 'file', name: obj.arg[0]};
            this.sendProcedure(out);
            break;
        case "ls":
            if (obj.l) {
                out['arguments'] = { "long?" : true };
            } else {
                out['arguments'] = {  };
            }
            this.devPool.setParamsForRealById(actId,{reqState:"LS"});
            this.sendProcedure(out);
            break;
        case "help":
            this.writeProcedure(this.getHelp(obj.arg[0]));
            break;
        default:
            this.writeProcedure(obj.cmd + " is unknown command!");
            break;
        }

        this.writeProcedure('');
    }
}

util.inherits(RbcLogic, EventEmitter);
module.exports = RbcLogic;
