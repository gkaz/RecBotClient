const fs = require('fs');
const yargs_parser = require('yargs-parser');

/*
  calculate byte's size to nice string human can read 
*/
function getCalcSizeStr(sizeVal) {

    const SZ_VALS = ['B','Kb','Mb'];
    const SZ_DV = 1024;
    let valKey = 0;
    let sz = sizeVal;

    for(valKey = 0; valKey < SZ_VALS.length; valKey++) {
        if(sz < SZ_DV)
            break;
        sz = sz / SZ_DV;
    }
    let strVal = Math.floor(sz * 10) / 10;
    return `${strVal} ${SZ_VALS[valKey]}`;
}

/*
    to have dirName existed
    succF - if the directory was created
    failF - fail existed and directory did not exist
*/
function configDir(dirName, succF, failF) {
	fs.access(dirName, function(err) {
	    if (err && err.code === 'ENOENT') {
	        fs.mkdir(dirName, (err)=>{
	        	if(err)
	        		failF();
	        	else
	        		succF();
	        });
	    }
	});
}

/*
    helps to parse cmd line
*/
function parseCmdLine(inpStr) {
    let parsedStr = yargs_parser(inpStr);
    parsedStr.cmd = parsedStr._[0] || '';
    // "_" (from yargs) has all the data, but arguments have idx >= 1
    parsedStr.arg = parsedStr._.slice(1);
    return parsedStr;
}

exports.getCalcSizeStr = getCalcSizeStr;
exports.configDir = configDir;
exports.parseCmdLine = parseCmdLine;