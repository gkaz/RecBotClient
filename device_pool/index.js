const DBG = require('../debug');
/*
    Every device connected to rbc has own properties and states.
    Active device's link points to the active device the application workng with.
*/
class Device {
    constructor(config={}) {
        this.dbg = config.dbg || console;
        this.configParams(config);
    }
    configParams(config={}) {
        this.deviceId = config.deviceId || '';
        this.deviceAlias= config.deviceAlias || '';
        this.reqState = config.reqState || '';  // the state when user cmd has been send
        this.cmdState = config.cmdState || Device.UNCONNECTED; // the state when command-notify has been received
        this.recSessState = config.recSessState || 'READY_REC_SESS_START';
    }
    // reset states only
    resetState() {
        this.dbg.log(DBG.INFORMATION,'reset State', this);
        this.configParams({
            deviceId: this.deviceId,
            deviceAlias: this.deviceAlias,
            dbg: this.dbg,
        });
        this.dbg.log(DBG.INFORMATION,'reset State (after cfg param)', this);
    }
}

Device.UNCONNECTED = 0;
Device.CONNECTED = 1;

class DevicePool {
    constructor(config={}) {
        this.dbg = config.dbg || console;
        this.devices = {};
        this.activeId = '*';

        this.addDevice(new Device({
            dbg: this.dbg,
            deviceId: '*',
            deviceAlias: 'broadcast',
        }));
        this.setActiveDeviceId('*');

    }
    addDevice(newDevice) {
        this.dbg.log(DBG.INFORMATION,'newDevice:', newDevice);
        if(newDevice instanceof Device) {
            if(!this.devices.hasOwnProperty(newDevice.deviceId)) {
                this.devices[newDevice.deviceId] = newDevice;
                return true;
            }
        }
    }
    removeDevice(rmDeviceId) {
        if(this.devices.hasOwnProperty(rmDeviceId)) {
            if(rmDeviceId === this.getActiveDeviceId() && '*' !== rmDeviceId) {
                this.setActiveDeviceId('*');
            }
            delete this.devices[rmDeviceId];
            return true;
        }
    }
    getDevice(askDeviceId) {
        if(this.devices.hasOwnProperty(askDeviceId)) {
            return this.devices[askDeviceId];
        }
    }
    setActiveDeviceId(activeDeviceId) {
        if(this.devices.hasOwnProperty(activeDeviceId)) {
            this.activeId = activeDeviceId;
            return true;
        }
    }
    getActiveDeviceId() {
        if(this.devices.hasOwnProperty(this.activeId)) {
            return this.activeId;
        } else {
            this.dbg.log(DBG.CRITICAL,"Device pool has active Id for unknown Device!", this.activeId);
        }
    }
    getActiveDevice() {
        return this.getDevice(this.activeId);
    }

    getDeviceIdList(all = true) {
        let devList = [];
        for(let d in this.devices) {
            if(all || '*' != d.deviceId) {
                devList.push(d);
            }
        }
        return devList;
    }
    /*
        if parameter all is false then output array excludes '*' (broadcast) device
    */
    getDeviceList(all = true) {
        let devList = [];
        for(let d in this.devices) {
            if(all || '*' != d.deviceId) {
                devList.push(this.devices[d]);
            }
        }
        return devList;
    }
    /*
        set params of device
    */
    setParams(devId, params) {
        let device = this.getDevice(devId);
        if(device) {
            for(let p in params) {
                if(device.hasOwnProperty(p)) {
                    device[p] = params[p];
                }
            }
        }
    }

    /*
        set params of some devices are listed in devIdList by Id
    */
    setParamsByList(devIdList, params) {
        for(let d of devIdList) {
            this.setParams(d, params);
        }
    }

    /*
        set params of real device or the group devices (if broadcast is active)
    */
    setParamsForRealById(actId, params) {
        let idList = [];
        if('*' === actId) {
            idList = this.getDeviceIdList(false);
        } else {
            idList.push(actId);
        }
        this.setParamsByList(idList,params);
        this.dbg.log(DBG.DEBUG, 'set params', idList, params);
    }


}
module.exports.Device = Device;
module.exports.DevicePool = DevicePool;