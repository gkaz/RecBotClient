const util = require('util');

class Debug {

	constructor(config={}) {

		/*this.EMERGENCY = 0;
		this.ALERT = 1;
		this.CRITICAL = 2;
		this.ERROR = 4;
		this.NOTICE = 5;
		this.INFORMATION = 6;
		this.DEBUG = 7;*/

		this.debugLevel = config.debugLevel || Debug.EMERGENCY;
		this.printObj = config.printObj;
	}

	checkLevel(level) {
		return level >= Debug.EMERGENCY && level <= Debug.DEBUG;
	}

	setDebugLevel(level) {
		if(this.checkLevel(level)) {
			this.debugLevel = level;
		}
	}

	getDebugLevel() {
		return this.debugLevel;
	}

	log(...params) {
		if(	typeof params === 'object' && params.length > 1) {
			let level = Number.parseInt(params[0]);
			if(!this.checkLevel(level)) {
				level = Debug.EMERGENCY;
			}
			if(level <= this.debugLevel) {
				if(null !== this.printOut) {
					let out = util.format.apply(this, params.slice(1)) + "\n";
					this.printObj.printOut(out);
				}
			}
		}
	}
}

Debug.EMERGENCY = 0;	
Debug.ALERT = 1;
Debug.CRITICAL = 2;		// critical errors
Debug.ERROR = 4;		// errors the system continues to work normally
Debug.NOTICE = 5;		// function calls
Debug.INFORMATION = 6;  // detailed notices OR users debug marks
Debug.DEBUG = 7;		// raw data

module.exports = Debug;
