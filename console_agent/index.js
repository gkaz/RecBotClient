/*
  Interactive console.
  Parses user's cmd and sends to cient
  Receives and prints client's response

*/
const os = require('os');
const util = require('util');
const readline = require('readline');
const EventEmitter = require('events').EventEmitter;

class ConsoleAgent {

    constructor() {
        process.stdin.setEncoding('utf8');
        this.promptPrefix = '';
        this.setMaxListeners(1);
    };

    /*
        prompt prefix will be displayed in start of the console prompt
    */
    setPromptPrefix(_promptPrefix) {
        this.promptPrefix = _promptPrefix + ' ';
    }

    reWriteStrToConsole(mess) {
        readline.clearLine(process.stdout, 0);
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(mess);
    };

    writeStrToConsole(mess) {
        this.reWriteStrToConsole(mess + os.EOL + this.promptPrefix + '> ');
    };

    /*
        Wrapper to universal calling string's print method using "printOut" name.
        Like "interface".
     */
    printOut(mess) {
        this.writeStrToConsole(mess);
    }

    writeArrToConsole(arr) {
        if(typeof arr === 'object' && Array.isArray(arr)) {
            arr.forEach((el)=>{

                if(typeof arr === 'object') { //????
                    el = this.getJSONAsStr(el);
                }
                this.writeStrToConsole(el);
            });
        }
    };

    writeJSONtoConsole(json_data) {
        // Object.prototype.toString.call
        if(typeof json_data === 'object') {
            let keys = Object.keys(json_data);
            keys.forEach((key)=>{
                this.writeStrToConsole(`${key}: ${JSON.stringify(json_data[key])}`);
            });  
        }
    }

    getJSONAsStr(json_data, delim=" ") {
        let data = [];
        if(typeof json_data === 'object') {
            //console.log('obj');
            for (let key in json_data) {
                data.push(json_data[key]);
            }
            return data.join(delim);
        }
        return json_data.toString();
    }

    writeJSONtoConsoleAsStr(json_data) {
        if(typeof json_data === 'object') {
            for (let key in json_data) {
                this.writeStrToConsole(key + "\t" + this.getJSONAsStr(json_data[key],"\t"));
            }
        }
    }

    writeObj2Console(obj) {
        if(typeof obj === 'object') {
            if(Array.isArray(obj)) {
                this.writeArrToConsole(obj);
            } else {
                this.writeJSONtoConsoleAsStr(obj);
            }
        } else if(typeof obj === 'string') {
            this.writeStrToConsole(obj);
        }
    }

    /*
      console's cmd string is processed by 'userInputProc(str)' callback function
    */
    readFromConsole() {
        process.stdin.on('readable', () => {
            let chunk;

            while ((chunk = process.stdin.read()) !== null) {

                chunk = chunk.trim();
                this.emit ('user_input', chunk);
            }
        });
    }

    /*
        format output by table scheme
        <key1>      <key2>          <keyM>
        ----------- ----------- ... -----------
        <key1_val1> <key2_val1>     <keyM_val1>
        <key1_val2> <key2_val2>     <keyM_val2>
        ..          ..              ..
        <key1_valN> <key2_valN> ... <keyM_valN>
    */
    writeDataAsTable(arr) {

        let tableData = {
            columnsDelimeter: ' ', // str is printed before next column has printed
            columns: {}, // item is instance of columnTpl
        };

        let counter = 0;

        if(typeof arr === 'object' && Array.isArray(arr)) {
            arr.forEach((el)=>{
                if(typeof arr === 'object') {
                    for (let key in el) {
                        if(!tableData.columns.hasOwnProperty(key)) {
                            tableData.columns[key]={};
                            tableData.columns[key]['header'] = key;
                            tableData.columns[key]['maxWidth'] = key.toString().length;
                            tableData.columns[key]['data'] = [];
                        }
                        //console.log(key, el[key]);
                        tableData.columns[key]['data'][counter] = el[key] || '';
                        if(el[key].toString().length > tableData.columns[key].maxWidth) {
                            tableData.columns[key].maxWidth = el[key].toString().length;
                        }
                    }
                    counter++;
                }
            });
        }

        for(let i=0; i < counter; i++) {
            let str = '';
            for(let col in tableData.columns) {
                let objStr =  tableData.columns[col].data[i];
                if(objStr) {
                    objStr = objStr.toString();
                } else {
                    objStr = '';
                }

                str += objStr;
                let s = tableData.columns[col].maxWidth - objStr.length;
                if(s>0) {
                    str += " ".repeat(s);
                }
                str += tableData.columnsDelimeter;
            }
            str = str.substring(0,str.length-1);
            this.writeStrToConsole(str);
        }
        this.writeStrToConsole('');
    }
};

util.inherits(ConsoleAgent, EventEmitter);
module.exports = ConsoleAgent;
