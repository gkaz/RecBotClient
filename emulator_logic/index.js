/*
  Client logic.
  Receives decoded server's cmd and answer the response.

  // Next feature: Must be configured in various states that real harware might have
  Incoming and outcoming data - js object

*/

const fs = require('fs');
const path = require('path');

/*
    device has maximum of CAM_AMOUNT cameras
*/
const CAM_AMOUNT = 2;

class EmulatorLogic {

    constructor() {
        this.cmd_state = 0;
    }

    respTemplate(result="success", response) {
        let out = {
            "result": result,
            "response": {},
        };
        if(undefined !== response) {
            out["response"] = response;
        }
        return out;
    }

    resetState() {
        this.cmd_state = 0;
    }

    /*
      emulator responds to rbc
      req - incoming object (assoc array)
      return response, out - object)
    */
    prepareSendData(req) {
        //let request_cmd = req;
        console.log('emulator logic, prepareSendData:',req);
        let out = {};
        //let dir_list=[];
        let target_dir = '';
        switch (req['command']) {
        case "help":
            //{ "command": "help", "arguments": { "command" : "get" } }
            out = this.respTemplate("success","Available commands: rec, stop, ls, get, rm, conf, help");
            // out = JSON.stringify(out);
            break;
        case "conf":
            //{ "command": "conf", "arguments": { "preview" : "front": true, "rear": false } }
            out = this.respTemplate();
            // out = JSON.stringify(out);
            break;
        /*
        case "rec":
            out = this.respTemplate();
            if(req["arguments"] != {}) {
                if(req.arguments.cam_id) {
                    if(req.arguments.cam_id >= 0 && req.arguments.cam_id < CAM_AMOUNT) {
                        out.response = `recording has been started on cam: ${req.arguments.cam_id}`;
                    } else {
                        out = this.respTemplate('fails',`Wrong cam_id: ${req.arguments.cam_id}!`);
                    }
                } else {
                    out.response = `recording has been started on all the cams: ${CAM_AMOUNT}`;
                }
            }
            // out = JSON.stringify(out);
            break;
        */
        case "start":
            out = this.respTemplate();
            let devices =  ["0", "1"];
            if(req["arguments"] != {}) {
                if(req.arguments.hasOwnProperty('devices') && req.arguments.devices.length > 0 ) {
                    devices = req.arguments.devices;
                }
                switch(this.cmd_state) {
                case 0:
                    out = {
                        command: 'session-start',
                        session: {
                            'device-id': 'AABBCCDDEEFF', //${req.arguments.cam_id}, 
                            'timestamp': 9999999999999, //AABBCCDDEEFF_9999999999999
                            'device-alias': 'test'
                        },
                    }
                    this.cmd_state = 1;
                    break;
                case 1:
                    out = {
                        'command': 'switch-camera',
                        'current-camera': 1,
                        'session': {
                            'device-id': 'AABBCCDDEEFF', 
                            'timestamp': 9999999999999, 
                            'device-alias': 'test'
                        },
                    }
                    this.cmd_state = 2;
                    break;
                case 2:
                    out = {
                        'command': 'switch-camera',
                        'current-camera': 0,
                        'session': {
                            'device-id': 'AABBCCDDEEFF', 
                            'timestamp': 9999999999999, 
                            'device-alias': 'test'
                        },
                    }
                    this.resetState();
                    break;
                default:
                    break;
                }
            }
            // out = JSON.stringify(out);
            break;
        case "stop":
            //{ "command" : "stop", "arguments": {} }
            out = this.respTemplate("success","Sender - cam #N");
           // out = JSON.stringify(out);
            break;
        case "ls":
			out = this.respTemplate();
			target_dir = __dirname + path.sep + ".." + path.sep;
            let files = fs.readdirSync(target_dir);
			let files_info=[];
			const fattr = {'size':'size','datetime':'mtime'};
			if(undefined != files) {
				for (let i=0; i<files.length; i++) {
					let file = target_dir + files[i];
					
					let file_obj = {
                        name: files[i]
                    };
                    if(req.hasOwnProperty('arguments')) {
                        let stats = fs.statSync(file);
                        if(!stats.isDirectory()) {
                            if(req.arguments.hasOwnProperty('isLong') && true == req.arguments.isLong) {
                                for(let j in fattr) {
                                    if(stats.hasOwnProperty(fattr[j])) {
                                        file_obj[j] = stats[fattr[j]];    
                                    }
                                }
                            }
                        }
                    }
                    
                    files_info.push(file_obj);
				}
				out["response"] = files_info;
			}
			
			console.log(out);
			
			//{ "command": "ls", "arguments": { "long": true } }
			//out = this.respTemplate("success",dir_list);
            break;
        case "status":
            out = this.respTemplate('success', { "status": {"battery-level" : 98, 
                                                            "uptime" : 5031454, 
                                                            "recording?" : false, 
                                                            "current-camera": 0 } });
            break;
        case "get":
            //{ "command": "get", "arguments": { "name": "VID_20201213_165931CAM_FRONT.mp4" } }
			
			out = this.respTemplate("fail");
			console.log('args:',req.arguments);
			if(undefined != req.arguments.name) {
				target_dir = __dirname + path.sep + ".." + path.sep + req.arguments.name;
				//console.log('target:',target_dir);
				try {
					fs.accessSync(target_dir, fs.constants.R_OK);
					out = this.respTemplate('success', {"name":req.arguments.name});
				} catch (err) {
				out = this.respTemplate('fail', {"err":(err.name + ":" + err.message)});
				}
			}
			console.log('GET:',out);
			
				/*
					out = {
						"result":"fail",
						"arguments":{},
				};*/
				
			/*	
				
				out = {
					"result":"success",
					"arguments":req.arguments,
			};*/
            break;
        case "rm":
            // { "command": "rm", "arguments": { "name": "VID_20201213_165931CAM_FRONT.mp4" } }
            // { "command": "rm", "arguments": { "name": "*" } }
            out = this.respTemplate("fail");
            if(req.hasOwnProperty('arguments') && req.arguments.hasOwnProperty('name')) {
                let resp_str = '';
                if('*' == req.arguments.name) {
                    resp_str = 'All the files has been removed!';
                } else {
                    resp_str = `File ${req.arguments.name} has been removed!`;
                }
                out = this.respTemplate("success", resp_str);
            }
            //out = JSON.stringify(out);
            break;
        default:
            out = this.respTemplate("fail");
            //out = JSON.stringify(out);
            break;
        }
        return out;
    }
}

module.exports = EmulatorLogic;
